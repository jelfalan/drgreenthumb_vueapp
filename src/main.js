// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import fs from 'fs'
import Vuikit from 'vuikit'
import VuiIcons from '@vuikit/icons'

import GetPixels from 'get-pixels'
import ColorCube from './core/ColorCube'

import '@vuikit/theme'

Vue.use(router)
Vue.use(Vuikit)
Vue.use(VuiIcons)

Vue.config.productionTip = false
Object.defineProperty(Vue.prototype, '$GetPixels', { value: GetPixels })
Object.defineProperty(Vue.prototype, '$ColorCube', { value: ColorCube })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
