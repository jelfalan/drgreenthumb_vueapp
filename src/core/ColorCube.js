// *********************************************/
// ColorCube Histogram mapping addpated from:
// https://github.com/pixelogik/ColorCube/blob/master/Python/ColorCube.py
// **********************************************/

import imageJS from 'image-js'
// test image
import ganjaLeaf from '../assets/images/ganjaLeaf.jpg'

// Class LocalMaximum
class LocalMaximum {
  constructor (hitCount, cellIndex, red, green, blue) {
    // hit count of cell
    this.hitCount = hitCount
    // linear index of cell
    this.cellIndex = cellIndex
    // average color of cell
    this.red = red
    this.green = green
    this.blue = blue
  }
}

// Class CubeCell
class CubeCell {
  constructor () {
    // Count number of distinct hits
    this.hitCount = 0
    // accumulators for color components
    this.rAcc = 0
    this.gAcc = 0
    this.bAcc = 0
  }
}

// Class ColorCube
export default class ColorCube {
  constructor (resolution = 30, avoidColor = null, distinctThreshold = 0.2, brightThreshold = 0.6, cellCount = 0) {
    this.resolution = resolution
    this.distinctThreshold = distinctThreshold
    this.avoidColor = avoidColor
    this.brightThreshold = brightThreshold
    this.cellCount = resolution * resolution * resolution

    this.cells = this.createCells(this.cellCount)
    this.neighborIndices = [
      [0, 0, 0],
      [0, 0, 1],
      [0, 0, -1],

      [0, 1, 0],
      [0, 1, 1],
      [0, 1, -1],

      [0, -1, 0],
      [0, -1, 1],
      [0, -1, -1],

      [1, 0, 0],
      [1, 0, 1],
      [1, 0, -1],

      [1, 1, 0],
      [1, 1, 1],
      [1, 1, -1],

      [1, -1, 0],
      [1, -1, 1],
      [1, -1, -1],

      [-1, 0, 0],
      [-1, 0, 1],
      [-1, 0, -1],

      [-1, 1, 0],
      [-1, 1, 1],
      [-1, 1, -1],

      [-1, -1, 0],
      [-1, -1, 1],
      [-1, -1, -1]
    ]
  }
  //  ***********************************/
  // Private Class functions
  // ************************************/
  cellIndex (red, green, blue) {
    // Returns linear index for cell with given 3D index
    return (red + green * this.resolution + blue * this.resolution * this.resolution)
  }

  clearCells () {
    this.cells.forEach(c => {
      c.hitCount = 0
      c.rAcc = 0.0
      c.gAcc = 0.0
      c.bAcc = 0.0
    })
  }

  getColors (image) {
    var max = this.findLocalMaxima(image)

    if (this.avoidColor !== null) {
      max = this.filterTooSimilar(max)
    }

    max = this.filterDistinctMaxima(max)
    var colors = []
    max.array.forEach(n => {
      var red = n.red * 255.0
      var green = n.green * 255.0
      var blue = n.blue * 255.0
      colors.push([red, green, blue])
    })
    return colors
  }

  // findLocalMaxima
  async findLocalMaxima (image) {
    // reset cells
    this.clearCells()
    let $image = await imageJS.load(ganjaLeaf)
    //console.log('image result: ' + $image)
    // var $image = sharp(ganjaLeaf)
    // $image.options.
    // $image.options.raw()
  }

  // filterDistinctMaxima
  filterDistinctMaxima (maxima) {

  }
  // filterTooSimilar
  filterTooSimilar (maxima) {
  }
  // create cells helper function
  createCells (cellCount) {
    var cells = []
    for (let x = 0; x < cellCount; x++) {
      cells.push(new CubeCell())
    }
    return cells
  }
}

// module.exports = {
//     ColorCube : ColorCube
// }
